import Koa from "koa";
import bodyParser from 'koa-body'
import ProductRoutes from "./src/routes/productRoutes.js"
import mongoose from "mongoose";


const app = new Koa();

const mongoDbUrl = "mongodb+srv://testUser:testPassword@cluster0.soy6d.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"

try {
  mongoose.connect(mongoDbUrl, { useNewUrlParser: true })
} catch (e) {
  logger.error(e);
  throw new Error("mongo db cannot be connected");
}

// response
app.use(bodyParser());
app.use(ProductRoutes.routes()).use(ProductRoutes.allowedMethods());

app.use(async ctx => {
  ctx.body = 'Hello World';
});

app.listen(8080);