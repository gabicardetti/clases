import mongoose from "mongoose"

const schema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true

    },
    thumbnail: {
        type: String
    }
});

export default mongoose.model("Product", schema);