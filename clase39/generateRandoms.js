const calculateRandoms = (limit) => {

    const result = {};

    for (let i = 0; i < limit; i++) {
        const randomNumber = Math.floor(Math.random() * 1000);
        result[randomNumber] = result[randomNumber] != undefined ? result[randomNumber] + 1 : 1;
    }
    return result;
}

process.on('message', (limit) => {
    const randoms = calculateRandoms(limit);
    process.send(randoms);
});

process.send("ping");
