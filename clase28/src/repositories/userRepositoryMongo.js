import User from "../models/User.js"
import mongoose from "mongoose";

export default class UserRepositoryMongo {

    constructor() {
    }

    async getAll() {
        return User.find();
    }

    async save(dto) {
        console.log(dto)
        const user = new User({
            username: dto.username,
            password: dto.hashedPassword
        });

        return user.save();
    }

    async getById(id) {
        let user
        try {
            const mongoId = mongoose.Types.ObjectId(id);
            user = await User.findOne({ _id: { $eq: mongoId } });
        } catch (e) {
            return;
        }

        return user;
    }

    async getByUsername(username) {
        let user
        try {
            user = await User.findOne({ username: { $eq: username } });
        } catch (e) {
            return;
        }
        return user;
    }

}