
# Clase 38

## Antes de correr el proyecto

Tenemos que tener mongodb instalado y corriendo

## Correr proyecto

```bash
yarn install
node index.js
```

### Parametros opcionales

```bash
node index.js PUERTO FACEBOOK_CLIENT_ID FACEBOOK_SECRET_ID

node index.js 8081 asdw1234Casea123 123456asd7872
```

PUERTO: Puerto en el que corre la aplicacion por defecto 8080
FACEBOOK_CLIENT_ID: facebook client id por defecto una de prueba
FACEBOOK_SECRET_ID: facebook secret id por defecto una de prueba

## La ruta para ver todo seria

<http://localhost:8080/>

Dejo el postman para probar todas las rutas



### Usar grapql

entrar a la ruta http://localhost:8080/graphql

Para guardar un nuevo producto
```js
mutation {
  saveProduct(title: "nuevo", price: "123", thumbnail: "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/1200px-Image_created_with_a_mobile_phone.png") {title}
}
```

Para listar todos los productos

```js
query {
  products {
    title
    price
    thumbnail
  }

}
```