import nodemailer from "nodemailer";
import logger from "../logger.js";
import config from "../../config.js"

const transporter = nodemailer.createTransport({
  host: config.emailHost,
  port: config.emailPort,
  auth: {
    user: config.emailAuth.user,
    pass: config.emailAuth.pass,
  },
});

const sendEmail = (to, name, subject, text, html, type) => {
  const elQueRecibe = `${name} <${to}>`;
  logger.info(elQueRecibe);
  let message = {
    from: "sender@email.com",
    to: elQueRecibe,
    subject,
    text,
    html,
  };

  transporter.sendMail(message, (error, info) => {
    if (error) {
      logger.error("Error occurred");
      logger.error(error.message);
      return;
    }

    logger.info("Message sent" + type);
    logger.info(nodemailer.getTestMessageUrl(info));
  });
};

export const sendRegisterSucessMessage = (to, name) => {
  const text = `Su registro fue exitoso ${name}`;
  const html = `<p> Su registro fue exitoso ${name} </p>`;
  const subject = "Registro exitoso";
  return sendEmail(to, name, subject, text, html, "Register message");
};

export const sendLogIn = (to, name) => {
  const text = `Log In ${name}`;
  const html = `<p> Log In ${name} </p>`;
  const subject = "Registro exitoso";
  return sendEmail(to, name, subject, text, html, "Log in");
};

export const sendLogOut = (to, name) => {
  const text = `Log out ${name}`;
  const html = `<p> Log out ${name} </p>`;
  const subject = "Log out";
  return sendEmail(to, name, subject, text, html, "Log out");
};
