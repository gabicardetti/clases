import { Injectable } from '@nestjs/common';
import { EditProductDto, NewProductDto, Product } from 'src/models/Product';
import ProductRepositoryMongo from 'src/repository/productRepositoryMongo';
@Injectable()
export class ProductService {

    private readonly productRepository: ProductRepositoryMongo;

    constructor() {
        this.productRepository = new ProductRepositoryMongo();
    }

    async getProduct(id: string): Promise<Product> {
        return this.productRepository.getById(id);
    }

    async getAll(): Promise<Product[]> {
        return this.productRepository.getAll();
    }

    async create(productDto: NewProductDto): Promise<Product> {
        return this.productRepository.save(productDto);

    }

    async update(id: string, productDto: EditProductDto): Promise<any> {
        return this.productRepository.updateById(id, productDto);

    }

    async delete(id: string): Promise<any> {
        return this.productRepository.deleteById(id);
    }
}
