import { NestFactory } from '@nestjs/core';
import { AppModule } from './modules/app.module';
import * as mongoose from "mongoose"


const mongoUrl: string = "mongodb+srv://testUser:testPassword@cluster0.soy6d.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
async function bootstrap() {
  try {
    await mongoose.connect(
      mongoUrl,
      {
        useNewUrlParser: true,
        useFindAndModify: false,
        useUnifiedTopology: true
      })
  } catch (e) {
    console.log(e)
    throw new Error("mongo db cannot be connected");
  }
  const app = await NestFactory.create(AppModule);
  await app.listen(8080);
}
bootstrap();
