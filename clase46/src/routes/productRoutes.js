const baseProduct = "/productos";
import Router from "@koa/router"
import  koaBody from 'koa-body';

const router = new Router();
import * as ProductController from "../controllers/productController.js"


router.get(baseProduct + "/listar", ProductController.getAllProducts);

router.get(baseProduct + "/listar/:id", ProductController.getProductById);

router.post(baseProduct + "/guardar", ProductController.createNewProduct);

router.put(baseProduct + "/actualizar/:id", ProductController.updateProductById);

router.delete(baseProduct + "/borrar/:id", ProductController.deleteProductById);

export default router;