
# Clase 29

## Antes de correr el proyecto

Tenemos que tener mongodb instalado y corriendo

## Correr proyecto

```bash
yarn install
node index.js
```

### Parametros opcionales

```bash
node index.js PUERTO FACEBOOK_CLIENT_ID FACEBOOK_SECRET_ID

node index.js 8081 asdw1234Casea123 123456asd7872
```

PUERTO: Puerto en el que corre la aplicacion por defecto 8080
FACEBOOK_CLIENT_ID: facebook client id por defecto una de prueba
FACEBOOK_SECRET_ID: facebook secret id por defecto una de prueba

## La ruta para ver todo seria

<http://localhost:8080/>

Dejo el postman para probar todas las rutas

Y arme una ruta extra para consultar el historial del chat 

GET localhost:8080/api/chat

Agrege 3 enpoints mas que son

api/auth/login Para logearse
api/auth/logout Para deslogearse
api/me Para traer la informacion de la session

## Persistir session en mongo

Si se quiere probar local con solamente tener corriendo mongo db suficiente.

Si se quiere usar mongoAtlas deberiamos ir a el index.js y cambiar al valor de la variable
mongoDbUrl y conectarse a mongoAtlas por ejml

```js
const mongoDbUrl=mongodb+srv://<user>:<password>@cluster0.soy6d.mongodb.net/myFirstDatabase?retryWrites=true&w=majority
```

Lo dejo configurado con una db de prueba


## User pm2 y forever

### Forever

Para correr el proyecto usando forever podemos usar

```cli
yarn forever
```

para listar el proceso

```cli
    yarn foreverList
```

luego para stopearlo

```cli
    yarn foreverStop
```


### usar pm2

Para correr el proyecto usando pm2 podemos usar

modo FORK

```cli
yarn pm2Fork
```

para listar el proceso

```cli
yarn pm2List
```

luego para stopearlo

```cli
yarn pm2StopFork
```

### modo CLUSTER

```cli
yarn pm2Cluster
```

para listar el proceso

```cli
yarn pm2List
```

luego para stopearlo

```cli
yarn pm2StopCluster
```
