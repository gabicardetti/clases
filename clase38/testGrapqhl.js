import express from "express";
import { graphqlHTTP } from "express-graphql";
import { buildSchema } from "graphql";

const schema = buildSchema(`
    type Query {
        mensaje: String,
        products: [Product]
    }
    type Mutation {
        saveProduct(title: String!, price: String!, thumbnail: String!,): Product
    }
    type Product {
        title: String
        price: String
        thumbnail: String
    }
`);

const products = [];

const saveProduct = ({ title, price, thumbnail }) => {
  let product = { title, price, thumbnail };
  products.push(product);
  return product;
};

const root = {
  mensaje: () => "GrapQL: Ingrese Product",
  products: () => products,
  saveProduct: saveProduct,
};

const app = express();

app.use(
  "/graphql",
  graphqlHTTP({
    schema: schema,
    rootValue: root,
    graphiql: true,
  })
);

app.use(express.static("public"));
app.listen(8080, () => {
  console.log("Servidor funcionando");
});
