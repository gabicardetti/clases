import ProductRepository from "../repositories/productRepositoryMongo.js"

const repository = new ProductRepository();

export const getAllProducts = async () => repository.getAll();

export const generateNewProduct = async (title, price, thumbnail) => {
  const product = {};
  if (title) product.title = title;
  if (price) product.price = price;
  if (thumbnail) product.thumbnail = thumbnail;

  const savedProduct = await repository.save(product);
  return savedProduct;
};

export const getProductById = async (id) => {
  const product = await repository.getById(id);
  return product;
};

export const deleteProductById = async (id) => {
  const product = await repository.deleteById(id);
  return product;
};

export const updateProductById = async (id, title, price, thumbnail) => {
  const product = await repository.getById(id);
  if (!product) return;

  if (title) product.title = title;
  if (price) product.price = price;
  if (thumbnail) product.thumbnail = thumbnail;

  const updatedProduct = await repository.updateById(id, product);

  return updatedProduct;
};

