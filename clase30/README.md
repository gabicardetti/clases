
# Clase 30

## Antes de correr el proyecto

Tenemos que tener mongodb instalado y corriendo

### Configurar ngnix

Tenemos que tener ngnix instalado y corriendo con esta configuracion

```config
worker_processes  1;


events {
}


http {
    include       mime.types;
    default_type  application/octet-stream;

    upstream node_app {
        server 127.0.0.1:8081;
        server 127.0.0.1:8082 weight=4;
    }


    server {
        listen       80;
        server_name  localhost;

        location / {
            root   /usr/share/nginx/html;
            index  index.html index.htm;
        }

        location /info/ {
           proxy_pass http://node_app;
        }


        location /randoms/ {
           proxy_pass http://node_app;
        }

    }

}
```

## Correr proyecto

```bash
yarn install
yarn start


yarn stop
```

### Parametros opcionales

```bash
node index.js PUERTO FACEBOOK_CLIENT_ID FACEBOOK_SECRET_ID

node index.js 8081 asdw1234Casea123 123456asd7872
```

PUERTO: Puerto en el que corre la aplicacion por defecto 8080
FACEBOOK_CLIENT_ID: facebook client id por defecto una de prueba
FACEBOOK_SECRET_ID: facebook secret id por defecto una de prueba

## La ruta para ver todo seria

<http://localhost:8080/>

Dejo el postman para probar todas las rutas

Y arme una ruta extra para consultar el historial del chat 

GET localhost:8080/api/chat

Agrege 3 enpoints mas que son

api/auth/login Para logearse
api/auth/logout Para deslogearse
api/me Para traer la informacion de la session

## Persistir session en mongo

Si se quiere probar local con solamente tener corriendo mongo db suficiente.

Si se quiere usar mongoAtlas deberiamos ir a el index.js y cambiar al valor de la variable
mongoDbUrl y conectarse a mongoAtlas por ejml

```js
const mongoDbUrl=mongodb+srv://<user>:<password>@cluster0.soy6d.mongodb.net/myFirstDatabase?retryWrites=true&w=majority
```

Lo dejo configurado con una db de prueba
