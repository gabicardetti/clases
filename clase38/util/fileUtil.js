import { readFile, writeFile } from "./utils.js"
import logger from "../src/logger.js"


export class File {

    constructor(fileName) {
        this.filePath = "./" + fileName + ".txt";
    }


    async read() {
        let file;
        try {
            file = await readFile(this.filePath);
        } catch (err) {
            return [];
        }
        return file;
    }

    async save(object) {
        let newFileContent = object;

        try {
            let oldObjects = await this.read(this.filePath);
            if (!Array.isArray(oldObjects))
                oldObjects = JSON.parse(oldObjects);
            if (oldObjects.length == 0)
                newFileContent = [object];
            else {
                newFileContent = [...oldObjects, object]
            }
        } catch (e) {
            logger.error(e);
        }

        try {
            await writeFile(this.filePath, JSON.stringify(newFileContent));
        } catch (err) {
            logger.log(err)
        }
    }
}

