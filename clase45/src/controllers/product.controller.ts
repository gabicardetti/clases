import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Patch, Post } from '@nestjs/common';
import { EditProductDto, NewProductDto, Product } from 'src/models/Product';
import { ProductService } from 'src/services/product.service';

@Controller("/api/productos")
export class ProductController {
  constructor(private readonly productService: ProductService) { }

  @Get("/listar")
  async getAll(): Promise<Product[]> {
    const products = await this.productService.getAll();
    return products;
  }

  @Get("/listar/:id")
  async getProduct(@Param('id') id: string): Promise<Product> {
    const product = await this.productService.getProduct(id);
    if(!product) throw new HttpException('Producto no encontrado', HttpStatus.NOT_FOUND);
    return product;
  }


  @Post("/agregar")
  async create(@Body() body: NewProductDto): Promise<Product> {
    return await this.productService.create(body);
  }

  @Patch("/actulizar")
  update(@Param('id') id: string, @Body() body: EditProductDto): Promise<any> {
    const product = this.productService.update(id, body);
    if(!product) throw new HttpException('Producto no encontrado', HttpStatus.NOT_FOUND);
    return product;

  }

  @Delete("/borrar")
  delete(@Param('id') id: string): Promise<any> {
    const product = this.productService.delete(id);
    if(!product) throw new HttpException('Producto no encontrado', HttpStatus.NOT_FOUND);
    return product;
  }
}
