import { Product, ProductMongo } from "../models/Product"
import * as mongoose from "mongoose"

export default class ProductRepositoryMongo {

    constructor() {
    }

    async getAll(): Promise<Product[]> {
        const products = await ProductMongo.find();
        return this.parseProducts(products);
    }

    async save(dto: any): Promise<Product> {
        const product = new ProductMongo({
            timestamp: new Date().toISOString(),
            name: dto.name,
            description: dto.description,
            code: dto.code,
            photoUrl: dto.photoUrl,
            price: dto.price,
            stock: dto.stock,
        });

        return this.parseProduct(await product.save());
    }

    async getById(id: any): Promise<Product | undefined> {
        let product: Product;
        let mongoId;
        try {
            mongoId = mongoose.Types.ObjectId(id);

            const p = await ProductMongo.findOne({ _id: { $eq: mongoId } });
            if(!p) return undefined;
            product = this.parseProduct(p);
        } catch (e) {
            console.log(e)
            return undefined;
        }
        return product;
    }

    async deleteById(id: any) {
        let product;
        try {
            const mongoId = mongoose.Types.ObjectId(id);
            product = await ProductMongo.deleteOne({ _id: { $eq: mongoId } });
        } catch (e) {
            return;
        }

        return product;
    }

    async updateById(id: any, object: any) {
        let product;
        try {
            const mongoId = mongoose.Types.ObjectId(id);
            product = await ProductMongo.updateOne({ _id: { $eq: mongoId } }, object);
        } catch (e) {
            return;
        }

        return product;
    }

    private parseProducts(data: mongoose.Document<any, {}>[]): Product[] {
        const products: Product[] = [];
        data.map(d => {
            products.push(this.parseProduct(d));
        });
        return products;
    }

    private parseProduct(data: mongoose.Document<any, {}> ) {
        const product: Product = {
            id: data.get("_id"),
            timestamp: data.get("timestamp"),
            name: data.get("name"),
            description: data.get("description"),
            code: data.get("code"),
            photoUrl: data.get("photoUrl"),
            price: data.get("price"),
            stock: data.get("stock"),
        };

        return product;
    }
}