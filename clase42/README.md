
# Clase 42

## Antes de correr el proyecto

Tenemos que tener mongodb instalado y corriendo

## Correr proyecto

```bash
yarn install
node index.js
```

### Parametros opcionales

```bash
node index.js PUERTO FACEBOOK_CLIENT_ID FACEBOOK_SECRET_ID

node index.js 8081 asdw1234Casea123 123456asd7872
```

PUERTO: Puerto en el que corre la aplicacion por defecto 8080
FACEBOOK_CLIENT_ID: facebook client id por defecto una de prueba
FACEBOOK_SECRET_ID: facebook secret id por defecto una de prueba

## La ruta para ver todo seria

<http://localhost:8080/>

Dejo el postman para probar todas las rutas


## Entorno

para configurar el entorno tenemos que crear un archivo llamado
entorno.env usando la palabra entorno como production, develop o local
generamos este archivo copiando el archivo example.env y modifando sus propiedades

FACEBOOK_CLIENT_ID=
FACEBOOK_CLIENT_SECRET=

EMAIL_HOST=
EMAIL_PORT=

EMAIL_AUTH_USER=
EMAIL_AUTH_PASS=


para elegir que entorno usar seteamos la varible NODE_ENV a ejectutar el proyecto

dejo ejemplos en el package.json y para probarlos usar

```
yarn dev   --> correr entorno desarrollo
yarn prod  --> correr entorno producion
yarn start --> correr entorno local

```
