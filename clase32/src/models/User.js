import mongoose from "mongoose"

const schema = new mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true

    },
    profilePhoto: {
        type: String
    }
});

export default mongoose.model("User", schema);