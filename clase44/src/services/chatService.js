import ChatRepository from "../repositories/chatRepositoryMongo.js";
// import { sendMessageBySms } from "../services/smsService.js";

const repository = new ChatRepository();

export const saveNewMessage = (msg) => {
  if (msg.message.includes("administrador")) {
    const message = `${msg.date} Mensaje de ${msg.email}  dice que ${msg.message} `;
    // sendMessageBySms(message);
  }
  repository.save(msg);
};

export const getAllMessagesFromFile = async () => {
  const chatHistory = await repository.getAll();
  return chatHistory;
};
