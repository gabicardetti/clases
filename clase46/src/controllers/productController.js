import * as ProductService from "../services/productService.js";

export const getAllProducts = async (ctx) => {
    /**
     * Devuelve array de productos
     */
    const products = await ProductService.getAllProducts();
    if (products.length > 0) {
        ctx.body = { products };
    } else {
        ctx.throw(400, "No hay productos cargados");
    }
}

export const getProductById = async (ctx) => {
    /**
     * Devuelve producto por id
     */
    const productId = ctx.params.id;
    const product = await ProductService.getProductById(productId);

    if (product) {
        ctx.body = { product };
    } else {
        ctx.throw(400, "Producto no encontrado");
    }
}

export const createNewProduct = async (ctx) => {
    /**
     * Devuelve el producto incorporado
     */

    const { title, price, thumbnail } = ctx.request.body;

    const product = await ProductService.generateNewProduct(title, price, thumbnail);
    if (product) {
        ctx.body = product;
    } else {
        // TODO specify the error
        ctx.throw(400, "Producto no encontrado");
        ctx.res.status(400).send("Error al crear el producto")
    }
}


export const updateProductById = async (ctx) => {
    /**
     * Actualiza un producto
     */
    const productId = ctx.req.params.id;

    const { title, price, thumbnail } = ctx.request.body;
    const product = await ProductService.updateProductById(productId, title, price, thumbnail);

    if (!product)
        ctx.throw(400, "Producto no encontrado");
}

export const deleteProductById = async (ctx) => {
    /**
     * Elimina un producto
     */
    const productId = ctx.req.params.id;
    const product = await ProductService.deleteProductById(productId);

    if (!product)
        ctx.res.status(400).send({ error: "Producto no encontrado" });
}
