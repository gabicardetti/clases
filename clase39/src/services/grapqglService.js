import { buildSchema } from "graphql";
import { getAllProducts, generateNewProduct } from "./productService.js";

export const schemaGrapQl = buildSchema(`
    type Query {
        mensaje: String,
        products: [Product]
    }
    type Mutation {
        saveProduct(title: String!, price: String!, thumbnail: String!,): Product
    }
    type Product {
        title: String
        price: String
        thumbnail: String
    }
`);

const saveProduct = async ({ title, price, thumbnail }) => {
  const product = await generateNewProduct(title, price, thumbnail)
  return product;
};

export const rootGrapQLConfig = {
  mensaje: () => "GrapQL: Ingrese Product",
  products: () => getAllProducts(),
  saveProduct: saveProduct,
};
