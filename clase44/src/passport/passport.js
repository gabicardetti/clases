import passport from 'passport';
import passportLocal from 'passport-local';
import passportFacebook from 'passport-facebook';
import { sendLogIn, sendLogOut, sendRegisterSucessMessage } from "../services/emailService.js";

import { getUserByUsername, createNewUser } from "../services/userService.js"
import bcript from "bcryptjs"

import logger from "../logger.js"
import config from "../../config.js"

// const facebookClientID = process.argv[3] || "497199115059618";
// const facebookClientSecret = process.argv[4] || "53f3e0010da97c9418afd09339ec95b8";

const facebookClientID = config.facebookClientID;
const facebookClientSecret = config.facebookClientSecret;

const LocalStrategy = passportLocal.Strategy
const FacebookStrategy = passportFacebook.Strategy

//----------------
//
//    RECORDAR QUE POR DEFECTO PASSPORT TOMA USERNAME COMO DATO,
//    SI LO CAMBIAMOS (POR NOMBRE, MAIL U OTRA COSA) HAY QUE AGREGAR
//    EN EL MIDDLEWARE EL DATO usernameField: 'nombre'
//
//-------------------

passport.use('login', new LocalStrategy({
    passReqToCallback: true
},
    async function (req, username, password, done) {
        const user = await getUserByUsername(username);
        logger.info(user)
        if (!user)
            return done(null, false)

        if (!bcript.compareSync(password, user.password))
            return done(null, false)
        sendLogIn(user.email, user.username);
        return done(null, user);
    })
);

passport.use('register', new LocalStrategy({
    passReqToCallback: true
},
    async function (req, username, password, done) {
        const findOrCreateUser = async function () {
            //busco usuario
            const { username, password, email } = req.body
            const user = await getUserByUsername(username);
            if (user) return done(null, false)

            const newUser = await createNewUser(username, password, null, email);
            sendRegisterSucessMessage(email, username);
            return done(null, newUser);
        }
        process.nextTick(findOrCreateUser);
    })
)

passport.serializeUser(function (user, done) {
    done(null, user.username);
});

passport.deserializeUser(async function (username, done) {
    const user = await getUserByUsername(username);
    done(null, user);
});

passport.use('facebook', new FacebookStrategy({
    clientID: facebookClientID,
    clientSecret: facebookClientSecret,
    callbackURL: "/api/auth/facebook/callback",
    profileFields: ['id', 'displayName', 'name', 'email', 'picture.type(large)']
}, async (accessToken, refreshToken, profile, done) => {
    logger.info(profile.photos[0], profile.photos);

    console.log(profile.emails, profile.email)
    const newUser = await createNewUser(profile.displayName, profile.id, profile.photos[0].value, profile.emails[0]);

    return done(null, newUser);
}))

export default passport;