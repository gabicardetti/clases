import dotenv  from 'dotenv';
import path  from 'path';

dotenv.config({
    path: path.resolve(process.cwd(), process.env.NODE_ENV + '.env')
});

const facebookClientID = process.argv[3] | process.env.FACEBOOK_CLIENT_ID;
const facebookClientSecret =
  process.argv[4] | process.env.FACEBOOK_CLIENT_SECRET;

const emailHost = process.env.EMAIL_HOST;
const emailPort = process.env.EMAIL_PORT;
const emailAuth = {
    user: process.env.EMAIL_AUTH_USER,
    pass: process.env.EMAIL_AUTH_PASS 
  };

const config = {
    facebookClientID,
    facebookClientSecret,
    emailHost,
    emailPort,
    emailAuth
};

export default config;