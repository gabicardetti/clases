import * as mongoose from 'mongoose';

export interface Product {
    id?: string,
    timestamp: Date,
    name: string,
    description: string,
    code: string,
    photoUrl: string,
    price: number,
    stock: number,
}

export interface ProductMongoI extends mongoose.Document{
    timestamp: Date,
    name: string,
    description: string,
    code: string,
    photoUrl: string,
    price: number,
    stock: number,
}



export interface EditProductDto {
    timestamp?: Date,
    name?: string,
    description?: string,
    code?: string,
    photoUrl?: string,
    price?: number,
    stock?: number,
}

export interface NewProductDto {
    name: string,
    description: string,
    code: string,
    photoUrl: string,
    price: number,
    stock: number,
}

export const productSchema = new mongoose.Schema({
    timestamp: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    code: {
        type: String,
        required: true
    },
    photoUrl: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true

    },
    stock: {
        type: Number,
    }
});

export const ProductMongo = mongoose.model("Product", productSchema);