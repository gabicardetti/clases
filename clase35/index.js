import express from "express";
import mongoose from "mongoose";
import ProductRoutes from "./src/routes/productRoutes.js";
import ChatRoutes from "./src/routes/chatRoutes.js";
import AuthRoutes from "./src/routes/authRoutes.js"
import * as fs from "fs";
import * as path from "path";

import { getAllProducts } from "./src/services/productService.js";
import { saveNewMessage } from ".//src/services/chatService.js"

import { Server } from "socket.io";
import http from "http";
import session from "express-session"
import MongoStore from 'connect-mongo';
import passport from "./src/passport/passport.js"

import {cpus} from "os" 

import { fork } from "child_process"

import logger from "./src/logger.js"

const app = express();
const port = process.env.PORT || 8080;

const baseUrl = "/api";


const server = http.createServer(app);
const io = new Server(server);
app.locals.io = io;
const mongoDbUrl = "mongodb+srv://testUser:testPassword@cluster0.soy6d.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"

try {
  mongoose.connect(mongoDbUrl, { useNewUrlParser: true })
} catch (e) {
  logger.error(e);
  throw new Error("mongo db cannot be connected");
}
app.use(session({
  secret: "secret",
  resave: false,
  saveUninitialized: false,
  store: new MongoStore(
    {
      mongoUrl: mongoDbUrl,
      mongoOptions: {
        useNewUrlParser: true,
        useUnifiedTopology: true
      }
    }
  )
}))
app.use(passport.initialize());
app.use(passport.session());

io.on("connection", async (socket) => {
  logger.info("a user connected");

  const products = await getAllProducts();
  io.emit("initialization", { products });

  socket.on("new message", (msg) => {
    io.emit("messages", msg);
    saveNewMessage(msg);

  });

});

app.use(express.json());
app.use(express.static("public"));

app.use(baseUrl + "/info" + "/artillery", (req, res) => {
  if(fs.existsSync("./testPerfomanceArtillery.txt")) {
    const route = path.resolve("./testPerfomanceArtillery.txt");
    res.sendFile(route);
} else{
    const errorMessage = {
      message :`Debe correr el siguiente comando para generar el archivo:
      node index.js y despues 
      artillery quick --count 20 -n 50 "http://localhost:8080/api/info" > testPerfomanceArtillery.txt`
    };
    res.send(errorMessage);
  }
});

app.use(baseUrl + "/info", (req, res) => {

  const info = {
    arguments: process.argv,
    osName: process.platform,
    nodeVersion: process.version,
    memoryUsage: process.memoryUsage,
    id: process.pid,
    numCpu: cpus().length
  }
  res.send(info);
});


app.use(baseUrl + "/randoms", (req, res) => {


  const { limit = 100000000 } = req.query;

  const compute = fork("./generateRandoms.js");
  compute.send(limit);
  compute.on('message', numbers => {
    if (numbers == "ping") compute.send(limit);
    else res.send(numbers);
  });

});

app.use(baseUrl, AuthRoutes)
app.use(baseUrl, ProductRoutes);
app.use(baseUrl, ChatRoutes);

server.listen(port, async () => {
  logger.info(`app listening at http://localhost:${port}`);
});


