import assert from "assert";
import axios from "axios";
//otras comunmente utilizadas son Proxyquire y sinon

const productUrl = 'http://localhost:8080/api/productos';

const createProduct = product => axios.post(`${productUrl}/guardar`, product);
const getProductById = (id) => axios(`${productUrl}/listar/${id}`);
const updateProduct = (id, product) => axios.put(`${productUrl}/actualizar/${id}`, product);


const productDto = {
    title: "nuevo producto",
    price: 120,
    thumbnail: "ss"
}

describe('Test product controller', () => {

    it('Crear un producto', async () => {

        let productCreated = await createProduct(productDto);
        productCreated = productCreated.data
        assert.ok(productCreated);
        
        assert.strictEqual(productDto.title, productCreated.title);
        assert.strictEqual(productDto.price, productCreated.price);
        assert.strictEqual(productDto.thumbnail, productCreated.thumbnail);
        assert.ok(productCreated._id);

    });

    it('Traer producto', async () => {

        let productCreated = await createProduct(productDto);
        productCreated = productCreated.data

        assert.ok(productCreated);


        let product = await getProductById(productCreated._id);
        product = product.data.product
        assert.ok(product);

        assert.strictEqual(product._id, productCreated._id);
        assert.strictEqual(productDto.title, product.title);
        assert.strictEqual(productDto.price, product.price);
        assert.strictEqual(productDto.thumbnail, product.thumbnail);

    })

    it('Actulizar producto', async () => {

        let productCreated = await createProduct(productDto);
        productCreated = productCreated.data

        assert.ok(productCreated);

        const updateProductDto = {
            title: "Producto editado",
            price: 999,
            thumbnail: "ssFFF"
        }

        let editedProduct = await updateProduct(productCreated._id, updateProductDto);
        assert.ok(editedProduct);

        let product = await getProductById(productCreated._id);
        product = product.data.product
        assert.ok(product);

        assert.strictEqual(product._id, productCreated._id);
        assert.strictEqual(updateProductDto.title, product.title);
        assert.strictEqual(updateProductDto.price, product.price);
        assert.strictEqual(updateProductDto.thumbnail, product.thumbnail);

    })
})